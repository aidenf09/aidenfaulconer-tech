/*
Auto-generated by: https://github.com/pmndrs/gltfjsx
*/

import React, {
  useRef, useMemo, useState, Component,
} from 'react';
import { useGLTF, Merged, useAnimations } from '@react-three/drei';

import hand_import from '../../../static/assets/gameModels/webexperience.gltf';// massive import, slows down FCP
import { useStore } from '../../store/store';

// useGLTF.preload(hand_import);//slows down first contentful paint

export default function Model({ instances, ...props }) {
  const group = useRef();
  const { nodes, materials, animations } = useGLTF(hand_import);
  const { actions } = useAnimations(animations, group);

  // pull in selection input from store
  // const { selected } = useStore((state) => state.appContext);

  // ========================================================================== //
  //   hand controls
  // ========================================================================== //
  const { animationsPlaying, propsUsing } = useStore((state) => state.threejsContext.context.hand);
  const _actions = ['wave', 'hold', 'snap', 'write', 'build'];
  React.useEffect(() => {
    if (process.env.development) {
      console.log('===== Nodes =====');
      console.log(nodes);
      console.log('===== Group =====');
      console.log(group.current);
      console.log('===== Materials =====');
      console.log(materials);
      console.log('===== Actions =====');
      console.log(actions);
    }

    // get availible animations
    const {
      wave, hold, snap, write, build,
    } = actions;
    // fadeIn(duration)
    // fadeOut(duration)
    // play()
    // reset()
    // stop()
    // stopFading()
    // halt()

    // toggle animations based on user input/scroll input
    // map through array with names, if name is not found, toggle off from actions
    _actions.map((availibleAction) => {
      if (animationsPlaying.includes(availibleAction)) {
        actions[availibleAction]?.reset();
        actions[availibleAction]?.fadeIn(0.015);
        actions[availibleAction]?.play();
      } else {
        actions[availibleAction]?.fadeOut(0.015);
        actions[availibleAction]?.reset();
      }
    });

    // toggle animated hands props, conencted to objectBone.L
    // depending on user input/scroll input
    // items start at scale 0, then scale up to 1 when popping in and out
    // switch statement

    // switch (selection) {
    //   case
    // }
  }, [group, animationsPlaying]);

  // ========================================================================== //
  //   props
  // ========================================================================== //
  const determineProp = React.useCallback(() => {
    const props = {
      Iphone: () => (
        <skinnedMesh
          name="iphone_13"
          geometry={nodes.iphone_13.geometry}
          material={nodes.iphone_13.material}
          skeleton={nodes.iphone_13.skeleton}
        />
      ),
      Emoji: () => (
        <skinnedMesh
          name="Emoji"
          geometry={nodes.Emoji.geometry}
          material={nodes.Emoji.material}
          skeleton={nodes.Emoji.skeleton}
        />
      ),
      Hammer: () => (
        <group name="Hammer">
          <skinnedMesh
            name="Cylinder002"
            geometry={nodes.Cylinder002.geometry}
            material={nodes.Cylinder002.material}
            skeleton={nodes.Cylinder002.skeleton}
          />
          <skinnedMesh
            name="Cylinder002_1"
            geometry={nodes.Cylinder002_1.geometry}
            material={nodes.Cylinder002_1.material}
            skeleton={nodes.Cylinder002_1.skeleton}
          />
        </group>
      ),
      Pencil: () => (
        <group name="pencil">
          <skinnedMesh
            name="Cylinder003"
            geometry={nodes.Cylinder003.geometry}
            material={nodes.Cylinder003.material}
            skeleton={nodes.Cylinder003.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_1"
            geometry={nodes.Cylinder003_1.geometry}
            material={nodes.Cylinder003_1.material}
            skeleton={nodes.Cylinder003_1.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_2"
            geometry={nodes.Cylinder003_2.geometry}
            material={nodes.Cylinder003_2.material}
            skeleton={nodes.Cylinder003_2.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_3"
            geometry={nodes.Cylinder003_3.geometry}
            material={nodes.Cylinder003_3.material}
            skeleton={nodes.Cylinder003_3.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_4"
            geometry={nodes.Cylinder003_4.geometry}
            material={nodes.Cylinder003_4.material}
            skeleton={nodes.Cylinder003_4.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_5"
            geometry={nodes.Cylinder003_5.geometry}
            material={nodes.Cylinder003_5.material}
            skeleton={nodes.Cylinder003_5.skeleton}
          />
          <skinnedMesh
            name="Cylinder003_6"
            geometry={nodes.Cylinder003_6.geometry}
            material={materials.eraser}
            skeleton={nodes.Cylinder003_6.skeleton}
          />
        </group>
      ),
      VR: () => (
        <group name="vr_headsest">
          <skinnedMesh
            name="Cube003"
            geometry={nodes.Cube003.geometry}
            material={nodes.Cube003.material}
            skeleton={nodes.Cube003.skeleton}
          />
          <skinnedMesh
            name="Cube003_1"
            geometry={nodes.Cube003_1.geometry}
            material={nodes.Cube003_1.material}
            skeleton={nodes.Cube003_1.skeleton}
          />
        </group>
      ),
      Paper: () => (
        <group
          name="paper"
          position={[-2.161, -0.937, 0.85]}
          rotation={[-2.989, 1.173, 3.006]}
          scale={[0.752, 0.752, 0.752]}
        >
          <mesh
            name="Plane"
            castShadow
            receiveShadow
            geometry={nodes.Plane.geometry}
            material={materials.Material}
          />
          <mesh
            name="Plane_1"
            castShadow
            receiveShadow
            geometry={nodes.Plane_1.geometry}
            material={nodes.Plane_1.material}
          />
          <mesh
            name="Checkmark"
            castShadow
            receiveShadow
            geometry={nodes.Checkmark.geometry}
            material={nodes.Checkmark.material}
            position={[-0.8, 0.125, 1.048]}
            scale={[249.691, 249.691, 249.691]}
          />
        </group>
      ),
    };

    return propsUsing.map((propName) => (React.createElement(props[propName])));
  }, [propsUsing]);

  // ========================================================================== //
  //   Web experience hand
  // ========================================================================== //
  return (
    <group
      ref={group}
      {...props}
      dispose={null}
      scale={[1.35, 1.35, 1.35, 1.35]}
      rotation={[0, Math.PI, 0]}
    >
      <group name="Scene">
        <group name="handRig">
          <primitive object={nodes.forearmL} />
          <skinnedMesh
            name="cuffs"
            geometry={nodes.cuffs.geometry}
            material={materials['Procedural Simple Cloth']}
            skeleton={nodes.cuffs.skeleton}
          />
          <skinnedMesh
            name="hand"
            geometry={nodes.hand.geometry}
            material={nodes.hand.material}
            skeleton={nodes.hand.skeleton}
          />

          <skinnedMesh
            name="watch"
            geometry={nodes.watch.geometry}
            material={nodes.watch.material}
            skeleton={nodes.watch.skeleton}
          />
        </group>
        {/**
        // ========================================================================== //
        //       Props
        // ========================================================================== //
       */}
        {propsUsing.length > 0 && (
        <>
          {determineProp()}
        </>
        )}
        {/**
          // ========================================================================== //
          //       Three point lighting setup
          // ========================================================================== //
       */}
        {/* <spotLight
          intensity={0.5}
          color="ffffff"
          name="TriLamp-Back"
          position={[-4.395, 5, 0.913]}
        />
        <spotLight
          intensity={0.25}
          color="ffffff"
          name="TriLamp-Fill"
          position={[4.487, 5, -0.136]}
        />
        <spotLight
          intensity={1.5}
          color="ffffff"
          name="TriLamp-Key"
          position={[1.589, 5, 4.198]}
        /> */}
      </group>
    </group>
  );
}

// Script to rename vertex groups in blender
// import bpy

// for obj in bpy.context.selected_objects:
//     if obj.type == "ARMATURE":
//         prefix = obj.name + "_"
//         for bone in obj.data.bones:
//             bone.name = prefix + bone.name

//     if obj.type == "MESH":
//         prefix = obj.parent.name + "_"
//         for vg in obj.vertex_groups:
//             if vg.name[0:len(prefix)] != prefix:
//                 vg.name = prefix + vg.name

// list all keyframes of an action

// import bpy

// ob = bpy.context.object

// if ob is not None:
//     if ob.animation_data is not None and ob.animation_data.action is not None:
//         action = ob.animation_data.action

//         print()
//         print("Keyframes")
//         print("---------")
//         for fcu in action.fcurves:
//             print()
//             print(fcu.data_path, fcu.array_index)
//             for kp in fcu.keyframe_points:
//                 print("  Frame %s: %s" % (kp.co[:]))

//         print()
//         print("Frames")
//         print("------")
//         for fcu in action.fcurves:
//             print()
//             print(fcu.data_path, fcu.array_index)
//             for i in range(1, 6):
//                 print("  Frame %i: %.6f" % (i, fcu.evaluate(i)))
